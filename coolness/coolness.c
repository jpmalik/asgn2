#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

double calculate_coolness(double temperature, double velocity) {
    return 35.74 + 0.6315 * temperature - 35.75 * pow(velocity, 0.16) + 0.4275 * temperature * pow(velocity, 0.16);
}

int main(int argc, char *argv[]) {
    double temperature, velocity; 
    
    if (argc >= 4) {
        printf("Usage: ./coolness [temperature] [wind speed]\n");
        return 1;
    }

    if (argc == 1) {  // No additional arguments
        printf("Temp\tWind\tCoolness\n");
        for (temperature = -10.0; temperature <= 40.0; temperature += 10.0) {
            for (velocity = 5.0; velocity <= 15.0; velocity += 5.0) {
                printf("%0.1f\t%0.1f\t%0.1f\n", temperature, velocity, calculate_coolness(temperature, velocity));
            }
            printf("\n");
        }
    } else if (argc == 2) {
        temperature = atof(argv[1]); 
        if (temperature < -99 || temperature > 50) {
            printf("Error: Coolness. Acceptable input values are -99<=T<=50 and 0.5<=V.\n");
            return 1;
        }
        printf("Temp\tWind\tCoolness\n");
        printf("%0.1f\t5.0\t%0.1f\n", temperature, calculate_coolness(temperature, 5.0));
        printf("%0.1f\t10.0\t%0.1f\n", temperature, calculate_coolness(temperature, 10.0));
        printf("%0.1f\t15.0\t%0.1f\n", temperature, calculate_coolness(temperature, 15.0));
        return 0;
    } else if (argc == 3) {
        temperature = atof(argv[1]);
        velocity = atof(argv[2]);
        if (temperature < -99 || temperature > 50 || velocity < 0.5) {
            printf("Error: Coolness. Acceptable input values are -99<=T<=50 and 0.5<=V.\n");
            return 1;
        }
        printf("Temp\tWind\tCoolness\n");
        printf("%0.1f\t%0.1f\t%0.1f\n", temperature, velocity, calculate_coolness(temperature, velocity));
    }
    return 0;
}


    



