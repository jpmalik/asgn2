#!/bin/bash

# No arguments, should print a table of values
./coolness

# Invalid temperature value, should print an error message
./coolness 60.0 5.0

# Valid temperature and wind speed values
./coolness 20.0 5.0

# Valid temperature and wind speed values
./coolness -10.0 15.0

# Valid temperature and wind speed values
./coolness 0.0 0.5

# Valid temperature and wind speed values
./coolness 10.0 10.0

# Valid temperature and wind speed values
./coolness 25.0 15.0
