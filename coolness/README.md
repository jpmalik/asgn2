# Coolness
Coolness is a program for my 2nd lab in cse 13. In this lab we are taking in a few different amounts of arguments. We can take in no arguments, 1 argument, 2 arguments, anything. We are using temperature and speed inorder to calculate the coolness factor!  

Arguments
lets first look at arguments: 
- 0 arguments- for this we used predetermined values and provide outputs to the user 
- 1- for this we assume the input given is temperature and calculate differnt coolness values based on differnt wind speeds(predetermined)
- 2-for this we utilize the first input as temperature and the following input as speed to calculate the coolness value 

## Files 
- `coolness.c` - this is the c code file, this is the file that actually is the program 
- `testing.sh` - this where I put the testing
- `testing.out` - this is the output of the testing 
- `Makefile` - this file is to "make" the program using commands 

```bash
make 
./coolness [args]
```
