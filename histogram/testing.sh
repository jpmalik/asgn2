#!/bin/bash

# compilation
make

# Test cases
echo "Basic Input"
echo -e "1 2 3 4 5 6 7 8 9 10\n^D" | ./histogram


echo "Larger Values"
echo -e "100 200 300 400 500 600 700 800 900 1000\n^D" | ./histogram
echo "Varying Bin Sizes"
echo -e "1 2 4 8 16 32 64 128 256 512 1024\n^D" | ./histogram
echo "Exceeding Initial Range"
echo -e "1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192\n^D" | ./histogram
echo "Large Input Using Curly Braces"
echo -e "{1..10000}\n^D" | ./histogram
echo "Larger Values Using Curly Braces"
echo -e "{10000..20000}\n^D" | ./histogram

# exit
make clean
