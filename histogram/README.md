# Histogram

This program makes histograms from positive integers. With each additional number, it dynamically adjusts the bin size with each additionally larger number.

1. Compile: `make`
2. Run: `./histogram`
3. Input your positive integers (separated by spaces or tabs).
4. Hit `Ctrl+D` to say you're done.

- Type in your positive integers.
- It doesn't like negatives and assumes the smallest is zero.

**Files:**

- `.gitignore`: Ignore executables
- `histogram.c`: Main program client
- `Makefile`: Compilation file
- `testing.sh`: Testing for client file
- `testing.out`: Output for testing of client file


```bash
bash -v testing.sh >& testing.out
```
