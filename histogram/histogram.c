#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define BIN_MAX_SIZE 16

int main() {
  int size = 1, bins[BIN_MAX_SIZE] = {0};
  printf("%d bins of size %d for range [%d,%d)\n", BIN_MAX_SIZE, size, 0, size * BIN_MAX_SIZE);
  int input, scanNum = scanf("%d", &input);

  while (scanNum != EOF) {
    if (scanNum == 1) {
      while (input >= size * BIN_MAX_SIZE) {
        // Increase bin size
        for (int i = 0; i < BIN_MAX_SIZE / 2; i++) {
          int j = i * 2;
          bins[i] = bins[j] + bins[j + 1];
        }

        // Reset upper bins
        for (int i = BIN_MAX_SIZE / 2; i < BIN_MAX_SIZE; i++)
          bins[i] = 0;

        // Increase range
        size *= 2;

        // Note increased bin range
        printf("%d bins of size %d for range [%d,%d)\n", BIN_MAX_SIZE, size, 0,
               size * BIN_MAX_SIZE);
      }
      // Increase bin count
      if (input >= 0)
        bins[input / size]++;
    } else {
      // Discard non-valid and non-integer input
      scanf("%*s");
    }
    scanNum = scanf("%d", &input);
  }

  for (int i = 0; i < BIN_MAX_SIZE; i++) {
    int lowBo = i * size, highBo = (i + 1) * size - 1;
    printf("[%3d:%3d]", lowBo, highBo);

    // Print histogram and account for whitespace
    for (int j = 0; j < bins[i]; j++) {
      if (j == 0)
        printf(" ");
      printf("*");
    }

    printf("\n");
  }

  return 0;
}
